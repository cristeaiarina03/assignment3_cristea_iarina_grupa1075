
const FIRST_NAME = "Iarina";
const LAST_NAME = "Cristea";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
class Employee {

    // constructor(){
    //     this.name="";
    //     this.surname="";
    //     this.salary=""
    // }
    constructor(name,surname,salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

    getDetails(){
        return `${this.name} ${this.surname} ${this.salary}`;
    }
    
}

class SoftwareEngineer extends Employee{
    
       
    
    constructor(name,surname,salary,experience)
   {
       super(name,surname,salary);
       if(arguments[3]===undefined)
            this.experience="JUNIOR";
       else this.experience=experience;
   }

   applyBonus(){
       let salary=0;
       if(this.experience=="SENIOR")
            salary=this.salary+this.salary*0.2;
            else if(this.experience=="MIDDLE")
                salary=this.salary+this.salary*0.15; 
                    else 
                        salary=this.salary+this.salary*0.1;
                        
        return salary;                

   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

